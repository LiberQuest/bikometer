import 'package:flutter/material.dart';

import '../utilities/themes.dart';

class CoreProvider extends ChangeNotifier {
  ThemeData currentTheme = primaryTheme;

  void toggleTheme() {
    if(currentTheme == primaryTheme) {
      currentTheme = secondaryTheme;
    } 
    else if(currentTheme == secondaryTheme) {
      currentTheme = primaryTheme;
    }

    notifyListeners();
  }
}
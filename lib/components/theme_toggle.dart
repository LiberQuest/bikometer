import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:bikometer/provider/core_provider.dart';

class ThemeToggle extends StatefulWidget {
  const ThemeToggle({ Key? key }) : super(key: key);

  @override
  _ThemeToggleState createState() => _ThemeToggleState();
}

class _ThemeToggleState extends State<ThemeToggle> {
  bool isToggled = false;

  void handleToggle(bool value) {
    Provider.of<CoreProvider>(context, listen: false).toggleTheme();
    setState(() {
      isToggled = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    Icon themeSymbol = isToggled
      ? const Icon(Icons.light_mode)
      : const Icon(Icons.dark_mode);

    return Row(
      children: [
        themeSymbol,
        Switch(
          value: isToggled,
          onChanged: handleToggle,
        )
      ],
    );
  }
}